#include "stdafx.h"
#include "BlobTreeNode.h"

CBlobTreeNode::CBlobTreeNode(CBlobData *blob, int threshold)
{
	m_Children = NULL;
	m_Threshold = threshold;
	m_Node = blob;
}

CBlobTreeNode::~CBlobTreeNode()
{
	if (m_Node != NULL)
	{
		delete m_Node;
		m_Node = NULL;
	}
	if (m_Children != NULL)
	{
		for (int i = 0; i < (int)m_Children->size(); i++)
		{
			delete (*m_Children)[i];
			(*m_Children)[i] = NULL;
		}
		m_Children->clear();
		delete m_Children;
		m_Children = NULL;
	}
}