#pragma once
#include <vector>

using namespace std;

typedef struct intensity_pixel_set_tag
{
	unsigned short intensity;
	vector<int> *pixelList;
	intensity_pixel_set_tag(unsigned short inten, int pixel)
	{
		intensity = inten;
		pixelList = new vector<int>();
		pixelList->push_back(pixel);
	}
	~intensity_pixel_set_tag()
	{
		pixelList->clear();
		delete pixelList;
	}
} INTENSITY_PIXEL_SET;

