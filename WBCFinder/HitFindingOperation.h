#pragma once
#include "ColorType.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "BlobData.h"
#include "FindConnectedPixels.h"
#include "RegionDataSet.h"
#include "BlobTreeNode.h"

using namespace std;

#define SAMPLEFRAMEWIDTH  100
#define SAMPLEFRAMEHEIGHT 100

class CHitFindingOperation
{
protected:
	template<typename T> static void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> static void Swap(vector<T> *list, int index1, int index2);
	static int Partition(vector<int> *list, int lo, int hi);
	static int ChoosePivot(vector<int> *list, int lo, int hi);
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void FreeBlobList(vector<CBlobData *> *blobList);
	static void FreeBlobTree(vector<CBlobTreeNode *> *blobTree);
	static void GetBlobBoundary(CBlobData *blob);
	static void CalculateBoundary(vector<CBlobData *> *blobList);
	static void ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height);
	static void FindBlobs(unsigned short *image, vector<CBlobData *> *blobList, int *CPI);
	static void SortPixelIntensities(unsigned short *image, vector<INTENSITY_PIXEL_SET *> *sortedList);
	static void FindBlob(CBlobTreeNode *node);
	static void AddOnePixel(vector<INTENSITY_PIXEL_SET *> *sortedList, unsigned short intensity, int position);
	static void PickLeafNodes(vector<CBlobTreeNode *> *treeHead, vector<CBlobTreeNode *> *leafNodes);
	bool MergeBlobsToGetCTC(CRGNData *region, vector<CRGNData *> *rgnList);
	int GetOverlapPixelCount(vector<int> *blob1, vector<int> *blob2, vector<int> *blob3);
	bool blobIncluded(vector<int> *list, int blobIndex);
	void CalculateHitAttributes(CRGNData *region);
	int GetPixelCountSum(CRGNData *region, PIXEL_COLOR_TYPE color);
	int GetAverageIntensity(CRGNData *region, PIXEL_COLOR_TYPE color);
	bool ProcessOnePatch(CRGNData *region, vector<CRGNData *> *regionList);

public:
	CHitFindingOperation();
	virtual ~CHitFindingOperation();
	CLog *m_Log;
	bool ProcessOneRegion(CRGNData *region);
};

