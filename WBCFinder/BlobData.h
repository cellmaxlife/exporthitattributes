#pragma once
#include <vector>

using namespace std;

class CBlobData
{
public:
	CBlobData(int width, int height);
	CBlobData(CBlobData *blob);
	virtual ~CBlobData();
	int m_Width;
	int m_Height;
	vector<int> *m_Boundary;
	vector<int> *m_Pixels;
	vector<unsigned short> *m_Intensities;
};

