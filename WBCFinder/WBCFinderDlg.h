
// WBCFinderDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "HitFindingOperation.h"
#include "SingleChannelTIFFData.h"

using namespace std;


// CWBCFinderDlg dialog
class CWBCFinderDlg : public CDialogEx
{
// Construction
public:
	CWBCFinderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_WBCFINDER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	BYTE GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff);
	void PaintImages();
	
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	unsigned short *m_RedPatchImage;
	unsigned short *m_GreenPatchImage;
	unsigned short *m_BluePatchImage;
	unsigned short *m_RedZoomImage;
	unsigned short *m_GreenZoomImage;
	unsigned short *m_BlueZoomImage;
	CStatic m_Display;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedLoad();
	CString m_ImageFilename;
	int m_HitIndex;
	int m_TotalHits;
	afx_msg void OnBnClickedNexthit();
	afx_msg void OnBnClickedPrevhit();
	afx_msg void OnBnClickedGotohit();
	BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg LRESULT OnMyMessage(WPARAM wparam, LPARAM lparam);
	afx_msg LRESULT OnEnableButtons(WPARAM wparam, LPARAM lparam);
	CHitFindingOperation m_HitFinder;
	vector<CRGNData *> m_RGNDataList;
	void CopyToRGBImage();
	void UpdateImageDisplay();
	CLog m_Log;
	CImage m_Image;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	CButton m_ShowBoundary;
	CButton m_Blue;
	CButton m_Green;
	CButton m_Red;
	afx_msg void OnBnClickedRedcolor();
	afx_msg void OnBnClickedGreen();
	afx_msg void OnBnClickedBlue();
	afx_msg void OnBnClickedShow();
	void SaveRegionFile(CString pathname, CString filename);
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void EnableButtons(BOOL enable);
	CString m_FullImagePathname;
	afx_msg void OnBnClickedRunBatch();
	void SaveRegionData(CString pathname, CString filename);
	bool m_ZeissData;
	int m_RedCPI, m_GreenCPI, m_BlueCPI;
	bool ReadFilesAndRun(CString imagefile, CString *sampleName1, CString rgnFilename, int *ctcCount, float *ctcCapture, 
		CArchive &ctcCapturedArchive, int *ctcCapturedCount, CArchive &ctcNotCapturedArchive, int *ctcNotCapturedCount);
	BOOL LoadRegionData(CString pathname, vector<CRGNData *> *regionList);
	CString m_BatchFileName;
	CString Leica_Red_Prefix;
	CString Leica_Green_Prefix;
	CString Leica_Blue_Prefix;
	CString Zeiss_Red_Postfix;
	CString Zeiss_Green_Postfix;
	CString Zeiss_Blue_Postfix;
	void LoadDefaultSettings();
	void ExportTrainingData(CString regionFilename, CString sampleName, int *ctcCount, float *ctcCapture,
		CArchive &ctcCapturedArchive, int *ctcCapturedCount, CArchive &ctcNotCapturedArchive, int *ctcNotCapturedCount);
	CString m_Caption;
	void SetStatus(CString message);
	void UpdateHitCount(CString filename, int hitCount);
};
