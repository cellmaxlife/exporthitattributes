﻿
// WBCFinderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "WBCFinder.h"
#include "WBCFinderDlg.h"
#include "afxdialogex.h"
#include "SingleRegionImageData.h"
#include "shlwapi.h"
#pragma comment(lib, "shlwapi.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CWBCFinderDlg dialog
#define IMAGE_WINDOW_X0 15
#define IMAGE_WINDOW_Y0 54
#define IMAGE_WINDOW_X1 1028
#define IMAGE_WINDOW_Y1 900

#define NUM_IMAGE_COLUMNS	4
#define NUM_IMAGE_ROWS		4
#define NUM_IMAGES			16
#define WM_MY_MESSAGE (WM_USER+1001)
#define WM_ENABLE_BUTTONS (WM_USER+1002)

static DWORD WINAPI DisplayHitsOperation(LPVOID param);
static DWORD WINAPI BatchRunOperation(LPVOID param);

CWBCFinderDlg::CWBCFinderDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CWBCFinderDlg::IDD, pParent)
	, m_Caption(_T("ExportHitAttributes"))
	, m_ImageFilename(_T(""))
	, m_HitIndex(0)
	, m_TotalHits(0)
	, m_FullImagePathname(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWBCFinderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DISPLAY, m_Display);
	DDX_Text(pDX, IDC_HITIDX, m_HitIndex);
	DDX_Text(pDX, IDC_TOTALHITS, m_TotalHits);
	DDX_Control(pDX, IDC_SHOW, m_ShowBoundary);
	DDX_Control(pDX, IDC_BLUE, m_Blue);
	DDX_Control(pDX, IDC_GREEN, m_Green);
	DDX_Control(pDX, IDC_REDCOLOR, m_Red);
}

BEGIN_MESSAGE_MAP(CWBCFinderDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CWBCFinderDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_LOAD, &CWBCFinderDlg::OnBnClickedLoad)
	ON_BN_CLICKED(IDC_NEXTHIT, &CWBCFinderDlg::OnBnClickedNexthit)
	ON_BN_CLICKED(IDC_PREVHIT, &CWBCFinderDlg::OnBnClickedPrevhit)
	ON_BN_CLICKED(IDC_GOTOHIT, &CWBCFinderDlg::OnBnClickedGotohit)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
	ON_MESSAGE(WM_ENABLE_BUTTONS, OnEnableButtons)
	ON_BN_CLICKED(IDC_REDCOLOR, &CWBCFinderDlg::OnBnClickedRedcolor)
	ON_BN_CLICKED(IDC_GREEN, &CWBCFinderDlg::OnBnClickedGreen)
	ON_BN_CLICKED(IDC_BLUE, &CWBCFinderDlg::OnBnClickedBlue)
	ON_BN_CLICKED(IDC_SHOW, &CWBCFinderDlg::OnBnClickedShow)
	ON_BN_CLICKED(IDC_RUNBATCH, &CWBCFinderDlg::OnBnClickedRunBatch)
END_MESSAGE_MAP()


// CWBCFinderDlg message handlers

BOOL CWBCFinderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_Log.NewLog(m_Caption);
	m_HitFinder.m_Log = &m_Log;
	SetWindowText(m_Caption);
	m_ZeissData = false;
	LoadDefaultSettings();
	m_RedCPI = 250;
	m_GreenCPI = 250;
	m_BlueCPI = 250;
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_Red.SetCheck(BST_CHECKED);
	m_Green.SetCheck(BST_CHECKED);
	m_Blue.SetCheck(BST_CHECKED);
	m_ShowBoundary.SetCheck(BST_CHECKED);
	m_RedPatchImage = new unsigned short[9 * PATCH_WIDTH * PATCH_HEIGHT];
	m_GreenPatchImage = new unsigned short[9 * PATCH_WIDTH * PATCH_HEIGHT];
	m_BluePatchImage = new unsigned short[9 * PATCH_WIDTH * PATCH_HEIGHT];
	m_RedZoomImage = new unsigned short[9 * PATCH_WIDTH * PATCH_HEIGHT];
	m_GreenZoomImage = new unsigned short[9 * PATCH_WIDTH * PATCH_HEIGHT];
	m_BlueZoomImage = new unsigned short[9 * PATCH_WIDTH * PATCH_HEIGHT];
	m_Image.Create((NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH), -(NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT), 24);
	memset(m_Image.GetBits(), 0, sizeof(BYTE) * m_Image.GetPitch() * m_Image.GetHeight());
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CWBCFinderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PaintImages();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CWBCFinderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CWBCFinderDlg::PaintImages()
{
	if (m_Image == NULL)
		return;
	else if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		CPaintDC dc(&m_Display);
		CRect rect;
		m_Display.GetClientRect(&rect);
		dc.SetStretchBltMode(HALFTONE);
		m_Image.StretchBlt(dc.m_hDC, rect);
		CDC* pDC = m_Display.GetDC();
		CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
		CPen *pOldPen = pDC->SelectObject(&CursorPen);
		int blockWidth = (rect.right - rect.left + 1) / NUM_IMAGE_COLUMNS;
		for (int i = 1; i < NUM_IMAGE_COLUMNS; i++)
		{
			pDC->MoveTo(rect.left + i * blockWidth, rect.top);
			pDC->LineTo(rect.left + i * blockWidth, rect.bottom);
		}
		int blockHeight = (rect.bottom - rect.top + 1) / NUM_IMAGE_ROWS;
		for (int i = 1; i < NUM_IMAGE_ROWS; i++)
		{
			pDC->MoveTo(rect.left, rect.top + i * blockHeight);
			pDC->LineTo(rect.right, rect.top + i * blockHeight);
		}
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextAlign(TA_LEFT);
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		int length10um = 15 * blockWidth / 100;
		for (int i = 0; i < NUM_IMAGES; i++, startIndex++)
		{
			if (startIndex < m_TotalHits)
			{
				int ii = i / NUM_IMAGE_COLUMNS;
				int jj = i % NUM_IMAGE_COLUMNS;
				CRect rect1;
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 10;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				CString label;
				label.Format(_T("%d"), (startIndex + 1));
				pDC->SetTextColor(RGB(255, 0, 0));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 190;
				pDC->MoveTo(rect1.left, rect1.top);
				pDC->LineTo(rect1.left + length10um, rect1.top);
				pDC->SetTextColor(RGB(255, 255, 255));
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 170;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("10um"));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				pDC->SetTextColor(RGB(255, 0, 0));
				rect1.left = rect.left + jj * blockWidth + 190;
				rect1.top = rect.top + ii * blockHeight + 10;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("R:%d"), m_RGNDataList[startIndex]->m_RedValue);
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				pDC->SetTextColor(RGB(0, 255, 0));
				rect1.left = rect.left + jj * blockWidth + 190;
				rect1.top = rect.top + ii * blockHeight + 30;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("G:%d"), m_RGNDataList[startIndex]->m_GreenValue);
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
			}
		}
		pDC->SelectObject(pOldPen);
	}
}

void CWBCFinderDlg::UpdateImageDisplay()
{
	RECT rect;
	m_Display.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}


void CWBCFinderDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	m_Image.Destroy();
	FreeRGNList(&m_RGNDataList);
	delete m_RedZoomImage;
	delete m_GreenZoomImage;
	delete m_BlueZoomImage;
	delete m_RedPatchImage;
	delete m_GreenPatchImage;
	delete m_BluePatchImage;
	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	CDialogEx::OnCancel();
}


void CWBCFinderDlg::OnBnClickedLoad()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = NULL;
	EnableButtons(FALSE);
	m_ImageFilename = _T("");
	m_FullImagePathname = _T("");
	FreeRGNList(&m_RGNDataList);
	m_TotalHits = 0;
	m_HitIndex = 0;
	
	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Hit files (*.hit)|*.hit"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_FullImagePathname = filename;
		m_ImageFilename = dlg.GetFileName();
		SetStatus(filename);
		if (m_ImageFilename.Find(_T(".hit")) > 0)
		{
			HANDLE thread = ::CreateThread(NULL, 0, DisplayHitsOperation, this, CREATE_SUSPENDED, NULL);

			if (!thread)
			{
				AfxMessageBox(_T("Fail to create a thread for displaying hits"));
				message.Format(_T("Failed to create a thread for displaying hits"));
				m_Log.Message(message);
				btn = (CButton *)GetDlgItem(IDC_LOAD);
				btn->EnableWindow(TRUE);
				return;
			}

			::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
			::ResumeThread(thread);
			message.Format(_T("Launched a thread to display hits"));
			m_Log.Message(message);
		}
	}
	else
	{
		btn = (CButton *)GetDlgItem(IDC_LOAD);
		btn->EnableWindow(TRUE);
	}
}

void CWBCFinderDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CWBCFinderDlg::CopyToRGBImage()
{
	if (m_Image == NULL)
		return;
	if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		if ((m_HitIndex < 1) && (m_HitIndex > m_TotalHits))
			return;
		BYTE *pCursor = (BYTE*)m_Image.GetBits();
		int nPitch = m_Image.GetPitch();
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		for (int ImageLocationIndex = 0; ImageLocationIndex < NUM_IMAGES; ImageLocationIndex++, startIndex++)
		{
			if (startIndex < m_TotalHits)
			{
				CRGNData *rgnPtr = m_RGNDataList[startIndex];
				int regionWidth = rgnPtr->GetWidth();
				int regionHeight = rgnPtr->GetHeight();
				
				unsigned short *pBlueBuffer = m_RGNDataList[startIndex]->GetImage(BLUE_COLOR);
				unsigned short *pGreenBuffer = m_RGNDataList[startIndex]->GetImage(GREEN_COLOR);
				unsigned short *pRedBuffer = m_RGNDataList[startIndex]->GetImage(RED_COLOR);
				
				int blueMax = rgnPtr->GetCPI(BLUE_COLOR) + 1600;
				if (rgnPtr->m_BlueFrameMax > 0)
					blueMax = rgnPtr->m_BlueFrameMax;
				int redMax = rgnPtr->GetCPI(RED_COLOR) + 900;
				if (rgnPtr->m_RedFrameMax > 0)
					redMax = rgnPtr->m_RedFrameMax;
				int greenMax = rgnPtr->GetCPI(GREEN_COLOR) + 1200;
				if (rgnPtr->m_GreenFrameMax > 0)
					greenMax = rgnPtr->m_GreenFrameMax;

				BYTE bluePixelValue = 0;
				BYTE greenPixelValue = 0;
				BYTE redPixelValue = 0;

				for (int y = 0; y < regionHeight; y++)
				{
					for (int x = 0; x < regionWidth; x++)
					{
						if (m_Blue.GetCheck() == BST_CHECKED)
							bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, 20 + rgnPtr->GetCPI(BLUE_COLOR));
						else
							bluePixelValue = (BYTE)0;
						if (m_Green.GetCheck() == BST_CHECKED)
							greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, greenMax, 20 + rgnPtr->GetCPI(GREEN_COLOR));
						else
							greenPixelValue = (BYTE)0;
						if (m_Red.GetCheck() == BST_CHECKED)
							redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, 20 + rgnPtr->GetCPI(RED_COLOR));
						else
							redPixelValue = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = bluePixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = greenPixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = redPixelValue;
					}
				}

				if ((m_ShowBoundary.GetCheck() == BST_CHECKED) && 
					(rgnPtr->GetBlobData(RB_COLORS)->size() > 0) &&
					(((m_Red.GetCheck() == BST_CHECKED) && (m_Green.GetCheck() == BST_CHECKED)) ||
					((m_Red.GetCheck() == BST_CHECKED) && (m_Blue.GetCheck() == BST_CHECKED)) ||
					((m_Blue.GetCheck() == BST_CHECKED) && (m_Green.GetCheck() == BST_CHECKED))))
				{
					CBlobData *blob = NULL;
					blob = (*rgnPtr->GetBlobData(RB_COLORS))[0];
					for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
					{
						int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
						int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
					}
				}
				else if ((m_ShowBoundary.GetCheck() == BST_CHECKED) &&
					(rgnPtr->GetBlobData(RED_COLOR)->size() > 0) &&
					(m_Red.GetCheck() == BST_CHECKED) &&
					(m_Green.GetCheck() == BST_UNCHECKED) &&
					(m_Blue.GetCheck() == BST_UNCHECKED))
				{
					for (int i = 0; i < (int)rgnPtr->GetBlobData(RED_COLOR)->size(); i++)
					{
						CBlobData *blob = (*rgnPtr->GetBlobData(RED_COLOR))[i];
						for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
						{
							int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
							int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
						}
					}
				}
				else if ((m_ShowBoundary.GetCheck() == BST_CHECKED) &&
					(rgnPtr->GetBlobData(GREEN_COLOR)->size() > 0) &&
					(m_Red.GetCheck() == BST_UNCHECKED) &&
					(m_Green.GetCheck() == BST_CHECKED) &&
					(m_Blue.GetCheck() == BST_UNCHECKED))
				{
					for (int i = 0; i < (int)rgnPtr->GetBlobData(GREEN_COLOR)->size(); i++)
					{
						CBlobData *blob = (*rgnPtr->GetBlobData(GREEN_COLOR))[i];
						for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
						{
							int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
							int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
						}
					}
				}
				else if ((m_ShowBoundary.GetCheck() == BST_CHECKED) &&
					(rgnPtr->GetBlobData(BLUE_COLOR)->size() > 0) &&
					(m_Red.GetCheck() == BST_UNCHECKED) &&
					(m_Green.GetCheck() == BST_UNCHECKED) &&
					(m_Blue.GetCheck() == BST_CHECKED))
				{
					for (int i = 0; i < (int)rgnPtr->GetBlobData(BLUE_COLOR)->size(); i++)
					{
						CBlobData *blob = (*rgnPtr->GetBlobData(BLUE_COLOR))[i];
						for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
						{
							int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
							int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
						}
					}
				}
			}
			else
			{
				for (int y = 0; y < SAMPLEFRAMEHEIGHT; y++)
				{
					for (int x = 0; x < SAMPLEFRAMEWIDTH; x++)
					{
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = (BYTE)0;
					}
				}
			}
		}
	}
}

BYTE CWBCFinderDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int)contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

void CWBCFinderDlg::OnBnClickedNexthit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(FALSE);
	if ((m_Image != NULL) &&
		((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT))))
	{
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		int totalPages = (m_TotalHits / NUM_IMAGES) + 1;
		if ((startIndex + NUM_IMAGES) <= (totalPages * NUM_IMAGES))
		{
			m_HitIndex += NUM_IMAGES;
			UpdateData(FALSE);
			CopyToRGBImage();
			UpdateImageDisplay();
		}
	}
	if (m_HitIndex > NUM_IMAGES)
	{
		btn = (CButton *)GetDlgItem(IDC_PREVHIT);
		btn->EnableWindow(TRUE);
	}
	btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(TRUE);
}


void CWBCFinderDlg::OnBnClickedPrevhit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_Image != NULL) &&
		((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT))))
	{
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		if (startIndex >= NUM_IMAGES)
		{
			m_HitIndex -= NUM_IMAGES;
			UpdateData(FALSE);
			CopyToRGBImage();
			UpdateImageDisplay();
		}
		else
		{
			CButton *btn = (CButton *)GetDlgItem(IDC_PREVHIT);
			btn->EnableWindow(FALSE);
		}
	}
}


void CWBCFinderDlg::OnBnClickedGotohit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	if ((m_HitIndex > 0) && (m_HitIndex <= m_TotalHits))
	{
		CopyToRGBImage();
		UpdateImageDisplay();
	}
}

BOOL CWBCFinderDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		UpdateData(TRUE);
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT CWBCFinderDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

LRESULT CWBCFinderDlg::OnEnableButtons(WPARAM wparam, LPARAM lparam)
{
	EnableButtons(TRUE);
	return 0;
}


DWORD WINAPI DisplayHitsOperation(LPVOID param)
{
	CWBCFinderDlg *ptr = (CWBCFinderDlg *)param;
	ptr->FreeRGNList(&ptr->m_RGNDataList);
	CString message;
	CFile theFile;
	if (theFile.Open(ptr->m_FullImagePathname, CFile::modeRead))
	{
		CArchive archive(&theFile, CArchive::load);
		int version = 0;
		archive >> version;
		if (version != 4)
		{
			message.Format(_T("Hit File Version Number (=%d) != 4"), version);
			ptr->SetStatus(message);
			ptr->m_Log.Message(message);
		}
		else
		{
			int foundHits = 0;
			int totalCount = 0;
			archive >> totalCount;
			if (totalCount > 0)
			{
				int isZeissData;
				archive >> isZeissData;
				if (isZeissData == 0)
					ptr->m_ZeissData = false;
				else
					ptr->m_ZeissData = true;
				unsigned short CPI_Value;
				archive >> CPI_Value;
				ptr->m_RedCPI = (int)CPI_Value;
				archive >> CPI_Value;
				ptr->m_GreenCPI = (int)CPI_Value;
				archive >> CPI_Value;
				ptr->m_BlueCPI = (int)CPI_Value;
				int WBCAverage;
				archive >> WBCAverage;
				for (int i = 0; i < totalCount; i++)
				{
					CSingleRegionImageData *data = new CSingleRegionImageData();
					data->Serialize(archive);
					CRGNData *ptr1 = new CRGNData(data->m_RegionX0Pos + 100, data->m_RegionY0Pos + 100, PATCH_WIDTH, PATCH_HEIGHT);
					ptr1->m_HitIndex = data->m_HitIndex + 1;
					ptr1->SetColorCode(data->m_ColorCode);
					unsigned short *redImg = new unsigned short[PATCH_WIDTH * PATCH_HEIGHT];
					unsigned short *greenImg = new unsigned short[PATCH_WIDTH * PATCH_HEIGHT];
					unsigned short *blueImg = new unsigned short[PATCH_WIDTH * PATCH_HEIGHT];
					ptr1->SetImages(redImg, greenImg, blueImg);
					for (int j = 0; j < PATCH_HEIGHT; j++)
					{
						memcpy(redImg + PATCH_WIDTH * j, data->m_RedRegionImage + (3 * PATCH_WIDTH * (j + 100)) + 100, sizeof(unsigned short) * PATCH_WIDTH);
						memcpy(greenImg + PATCH_WIDTH * j, data->m_GreenRegionImage + (3 * PATCH_WIDTH * (j + 100)) + 100, sizeof(unsigned short) * PATCH_WIDTH);
						memcpy(blueImg + PATCH_WIDTH * j, data->m_BlueRegionImage + (3 * PATCH_WIDTH * (j + 100)) + 100, sizeof(unsigned short) * PATCH_WIDTH);
					}
					delete data;
					ptr1->SetCPI(RED_COLOR, ptr->m_RedCPI);
					ptr1->SetCPI(GREEN_COLOR, ptr->m_GreenCPI);
					ptr1->SetCPI(BLUE_COLOR, ptr->m_BlueCPI);
					ptr->m_RGNDataList.push_back(ptr1);
					if (ptr->m_HitFinder.ProcessOneRegion(ptr1))
						foundHits++;
				}
			}
			message.Format(_T("%s,TotalHits=%d,HitsFound=%d"), ptr->m_FullImagePathname, totalCount, foundHits);
			ptr->SetStatus(message);
		}
		archive.Close();
		theFile.Close();
		ptr->m_TotalHits = (int)ptr->m_RGNDataList.size();
		ptr->m_HitIndex = 1;
		::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ptr->CopyToRGBImage();
		ptr->UpdateImageDisplay();
	}
	::PostMessage(ptr->GetSafeHwnd(), WM_ENABLE_BUTTONS, NULL, NULL);
	return 0;
}

void  CWBCFinderDlg::EnableButtons(BOOL enable)
{
	CButton *btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_PREVHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GOTOHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_RUNBATCH);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SHOW);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_BLUE);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GREEN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_REDCOLOR);
	btn->EnableWindow(enable);
}

void CWBCFinderDlg::OnBnClickedRedcolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CWBCFinderDlg::OnBnClickedGreen()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CWBCFinderDlg::OnBnClickedBlue()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CWBCFinderDlg::OnBnClickedShow()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}

void CWBCFinderDlg::OnBnClickedRunBatch()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	EnableButtons(FALSE);
	
	CString message;
	m_BatchFileName = _T("");
	CFileDialog dlg(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("Batch files (*.txt)|*.txt"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_BatchFileName = dlg.GetPathName();
	}
	else
	{
		AfxMessageBox(_T("Failed to specify batch file for batch run"));
		message.Format(_T("Failed to specify batch file for batch run"));
		m_Log.Message(message);
		EnableButtons(TRUE);
		return;
	}

	HANDLE thread = ::CreateThread(NULL, 0, BatchRunOperation, this, CREATE_SUSPENDED, NULL);

	if (!thread)
	{
		AfxMessageBox(_T("Fail to create thread for batch run"));
		message.Format(_T("Failed to creat a thread for batch run"));
		m_Log.Message(message);
		EnableButtons(TRUE);
		return;
	}

	::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
	::ResumeThread(thread);
	message.Format(_T("Launched a thread for batch run"));
	m_Log.Message(message);
}

DWORD WINAPI BatchRunOperation(LPVOID param)
{
	CStdioFile theFile;
	CWBCFinderDlg *ptr = (CWBCFinderDlg *)param;
	CString message;
	bool success = true;

	if (theFile.Open(ptr->m_BatchFileName, CFile::modeRead | CFile::typeText))
	{
		CString batchFileName = ptr->m_BatchFileName;
		int batchExtIndex = batchFileName.Find(_T(".txt"));
		batchFileName.Format(_T("%s.csv"), batchFileName.Mid(0, batchExtIndex));
		CString textline;
		CStdioFile batchResFile;
		if (batchResFile.Open(batchFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("Sample,RegionFileName,ManualCTCCount,CTCCapture(%%)\n"));
			batchResFile.WriteString(textline);
			batchResFile.Close();
		}

		CStdioFile CTCDataFile;
		if (CTCDataFile.Open(_T(".\\CTCCaptured.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("HitIndex,Sample,RgnIdx,RBPixels,RedPixels,RBOverlaps,RedRelative,GreenRelative\n"));
			CTCDataFile.WriteString(textline);
			CTCDataFile.Close();
		}

		if (CTCDataFile.Open(_T(".\\CTCNotCaptured.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("HitIndex,Sample,RgnIdx\n"));
			CTCDataFile.WriteString(textline);
			CTCDataFile.Close();
		}

		CFile ctcCapturedHitFile;
		int ctcCapturedHitCount = 0;
		CFile ctcNotCapturedHitFile;
		int ctcNotCapturedHitCount = 0;

		if ((ctcCapturedHitFile.Open(_T(".\\CTCCapturedHitFile.hit"), CFile::modeCreate | CFile::modeWrite)) &&
			(ctcNotCapturedHitFile.Open(_T(".\\CTCNotCapturedHitFile.hit"), CFile::modeCreate | CFile::modeWrite)))
		{
			CArchive ctcCapturedHitArchive(&ctcCapturedHitFile, CArchive::store);
			CArchive ctcNotCapturedArchive(&ctcNotCapturedHitFile, CArchive::store);
			ctcCapturedHitArchive << (int)4; // Version Number
			ctcCapturedHitArchive << (int)0; // HitCount will update at the end
			ctcCapturedHitArchive << (int)1;
			unsigned short cpiValue = (unsigned short)250;
			ctcCapturedHitArchive << cpiValue;
			ctcCapturedHitArchive << cpiValue;
			ctcCapturedHitArchive << cpiValue;
			ctcCapturedHitArchive << (int)550;
			ctcNotCapturedArchive << (int)4; // Version Number
			ctcNotCapturedArchive << (int)0; // HitCount will update at the end
			ctcNotCapturedArchive << (int)1;
			ctcNotCapturedArchive << cpiValue;
			ctcNotCapturedArchive << cpiValue;
			ctcNotCapturedArchive << cpiValue;
			ctcNotCapturedArchive << (int)550;
			int lineIdx = 0;
			while (theFile.ReadString(textline))
			{
				lineIdx++;
				int index = textline.Find(_T(","));
				if (index == -1)
				{
					message.Format(_T("%s Line#%d Error: Fail to read pathname"), ptr->m_BatchFileName, lineIdx);
					ptr->SetStatus(message);
					ptr->m_Log.Message(message);
					success = false;
					break;
				}
				CString pathname = textline.Mid(0, index);
				textline = textline.Mid(index + 1);
				index = textline.Find(_T(","));
				if (index == -1)
				{
					message.Format(_T("%s Line#%d Error: Fail to read image filename"), ptr->m_BatchFileName, lineIdx);
					ptr->SetStatus(message);
					ptr->m_Log.Message(message);
					success = false;
					break;
				}
				CString imagefilename = textline.Mid(0, index);
				CString regionFileName = _T("");
				textline = textline.Mid(index + 1);
				index = textline.Find(_T(","));
				if (index == -1)
				{
					message.Format(_T("%s Line#%d Error: Fail to read region filename"), ptr->m_BatchFileName, lineIdx);
					ptr->SetStatus(message);
					ptr->m_Log.Message(message);
					success = false;
					break;
				}
				else
				{
					regionFileName = textline.Mid(0, index);
					message.Format(_T("%d)%s"), lineIdx, regionFileName);
					if (lineIdx == 1)
						ptr->SetStatus(message);
					ptr->m_Log.Message(message);
				}
				CString sampleName1 = _T("");
				int ctcCount = 0;
				float ctcCapture = 0.0F;
				bool ret = ptr->ReadFilesAndRun(pathname + _T("\\") + imagefilename, &sampleName1,
					pathname + _T("\\") + regionFileName, &ctcCount, &ctcCapture, ctcCapturedHitArchive, &ctcCapturedHitCount, ctcNotCapturedArchive, &ctcNotCapturedHitCount);
				if (!ret)
				{
					CString filename1;
					index = imagefilename.Find(ptr->Leica_Red_Prefix);
					if (index == -1)
					{
						index = imagefilename.Find(_T("_c2_ORG.tif"));
						if (index > -1)
						{
							ptr->Zeiss_Red_Postfix = _T("_c2_ORG.tif");
							ret = ptr->ReadFilesAndRun(pathname + _T("\\") + imagefilename, &sampleName1, pathname + _T("\\") + regionFileName, &ctcCount, 
								&ctcCapture, ctcCapturedHitArchive, &ctcCapturedHitCount, ctcNotCapturedArchive, &ctcNotCapturedHitCount);
						}
						else
						{
							index = imagefilename.Find(_T("_c3_ORG.tif"));
							if (index > -1)
							{
								ptr->Zeiss_Red_Postfix = _T("_c3_ORG.tif");
								ret = ptr->ReadFilesAndRun(pathname + _T("\\") + imagefilename, &sampleName1, pathname + _T("\\") + regionFileName, &ctcCount, 
									&ctcCapture, ctcCapturedHitArchive, &ctcCapturedHitCount, ctcNotCapturedArchive, &ctcNotCapturedHitCount);
							}
						}
					}
				}
				if (ret)
				{
					message.Format(_T("%s,%s,%d,%.2f\n"), sampleName1, regionFileName, ctcCount, ctcCapture);
					if (batchResFile.Open(batchFileName, CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
					{
						batchResFile.SeekToEnd();
						batchResFile.WriteString(message);
						batchResFile.Close();
					}
					message.Format(_T("%d)%s,%s,%d,%.2f%%"), lineIdx, sampleName1, regionFileName, ctcCount, ctcCapture);
					ptr->SetStatus(message);
					ptr->m_Log.Message(message);
				}
				else
				{
					success = false;
					message.Format(_T("%d)%s,%s,failed"), lineIdx, sampleName1, regionFileName);
					ptr->SetStatus(message);
					ptr->m_Log.Message(message);
				}
			}
			ctcCapturedHitArchive.Close();
			ctcCapturedHitFile.Close();
			ctcNotCapturedArchive.Close();
			ctcNotCapturedHitFile.Close();
			ptr->UpdateHitCount(_T(".\\CTCCapturedHitFile.hit"), ctcCapturedHitCount);
			ptr->UpdateHitCount(_T(".\\CTCNotCapturedHitFile.hit"), ctcNotCapturedHitCount);
		}
		theFile.Close();
	}

	if (success)
		message.Format(_T("Batch Run Completed successfully"));
	else
		message.Format(_T("Batch Run Failed, please check log file for the error happened"));
	ptr->SetStatus(message);
	ptr->EnableButtons(TRUE);
	return 0;
}

bool CWBCFinderDlg::ReadFilesAndRun(CString imagefile, CString *sampleName1, CString ctcRegionFileName, 
	int *ctcCount, float *ctcCapture, CArchive &ctcCapturedArchive, int *ctcCapturedHitCount, CArchive &ctcNotCapturedArchive, int *ctcNotCapturedHitCount)
{
	bool ret = false;
	CString message;
	bool loaded = false;
	CString safeFilename = _T("");
	CString pathname = _T("");

	while (true)
	{
		int index = imagefile.Find(Leica_Red_Prefix, 0);
		if (index == -1)
			index = imagefile.Find(Zeiss_Red_Postfix, 0);
		if (index == -1)
		{
			message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), 
				imagefile, Leica_Red_Prefix, Zeiss_Red_Postfix);
			m_Log.Message(message);
			break;
		}
		ret = m_RedTIFFData->LoadRawTIFFFile(imagefile);
		if (!ret)
		{
			message.Format(_T("Failed to load %s"), imagefile);
			m_Log.Message(message);
			break;
		}
		else
		{
			message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), imagefile, m_RedTIFFData->GetCPI());
			m_Log.Message(message);
			index = imagefile.Find(Leica_Red_Prefix, 0);
			if (index == -1)
			{
				WCHAR *ch1 = _T("\\");
				index = imagefile.ReverseFind(*ch1);
				pathname = imagefile.Mid(0, index);
				safeFilename = imagefile.Mid(index + 1);
				int postfixIndex = imagefile.Find(Zeiss_Red_Postfix);
				if (postfixIndex == -1)
				{
					message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), 
						imagefile, Leica_Red_Prefix, Zeiss_Red_Postfix);
					m_Log.Message(message);
					break;
				}
				else
				{
					index = safeFilename.Find(_T("_ORG.tif"));
					safeFilename = safeFilename.Mid(0, index);
					CString samplePathName = imagefile.Mid(0, postfixIndex);
					CString filename2 = samplePathName + Zeiss_Green_Postfix;
					ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
						m_Log.Message(message);
						filename2 = samplePathName + Zeiss_Blue_Postfix;
						ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
						if (ret)
						{
							m_ZeissData = true;
							message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
							m_Log.Message(message);
							loaded = true;
							WCHAR *ch1 = _T("\\");
							index = imagefile.ReverseFind(*ch1);
							CString RedZeissFileName = imagefile.Mid(index + 1);
							index = RedZeissFileName.Find(_T("_ORG.tif"));
							*sampleName1 = RedZeissFileName.Mid(0, index);
						}
						else
						{
							message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
							m_Log.Message(message);
							break;
						}
					}
					else
					{
						message.Format(_T("Failed to load Green TIFF File %s"), filename2);
						m_Log.Message(message);
						break;
					}
				}
			}
			else
			{
				WCHAR *ch1 = _T("\\");
				index = imagefile.ReverseFind(*ch1);
				pathname = imagefile.Mid(0, index);
				safeFilename = imagefile.Mid(index + 1);
				index = imagefile.Find(Leica_Red_Prefix, 0);
				CString postfix = safeFilename.Mid(Leica_Red_Prefix.GetLength());
				index = imagefile.Find(Leica_Red_Prefix, 0);
				CString pathname = imagefile.Mid(0, index);
				CString filename2;
				filename2.Format(_T("%s%s%s"), pathname, Leica_Green_Prefix, postfix);
				ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					message.Format(_T("Failed to load Green TIFF File %s"), filename2);
					m_Log.Message(message);
					break;
				}
				else
				{
					message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
					m_Log.Message(message);
					filename2.Format(_T("%s%s%s"), pathname, Leica_Blue_Prefix, postfix);
					ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (!ret)
					{
						message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
						m_Log.Message(message);
						break;
					}
					else
					{
						m_ZeissData = false;
						message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
						m_Log.Message(message);
						loaded = true;
						index = postfix.Find(_T(".tif"));
						safeFilename = postfix.Mid(0, index);
						*sampleName1 = safeFilename;
					}
				}
			}
		}

		if (loaded)
		{
			FreeRGNList(&m_RGNDataList);
			ExportTrainingData(ctcRegionFileName, *sampleName1, ctcCount, ctcCapture, ctcCapturedArchive, ctcCapturedHitCount,
				ctcNotCapturedArchive, ctcNotCapturedHitCount);
			message.Format(_T("ctcCaptureRate=%.2f%%"), *ctcCapture);
			m_Log.Message(message);
		}
		else
		{
			message.Format(_T("loaded=false"));
			m_Log.Message(message);
			ret = false;
		}
		break;
	}
	return ret;
}

void CWBCFinderDlg::ExportTrainingData(CString regionFilename, CString sampleName, int *ctcCount, float *ctcCapture,
	CArchive &ctcCapturedArchive, int *ctcCapturedCount, CArchive &ctcNotCapturedArchive, int *ctcNotCapturedCount)
{
	int ctcCount1 = 0;
	int ctcFoundCount = 0;
	if (LoadRegionData(regionFilename, &m_RGNDataList))
	{
		for (int i = 0; i < (int)m_RGNDataList.size(); i++)
		{
			unsigned int color = m_RGNDataList[i]->GetColorCode();
			if ((color == CTC) || (color == CTC2))
			{
				ctcCount1++;
				CSingleRegionImageData *data = new CSingleRegionImageData();
				int x0, y0;
				m_RGNDataList[i]->GetPosition(&x0, &y0);
				data->m_ColorCode = color;
				data->m_RegionWidth = 300;
				data->m_RegionHeight = 300;
				data->m_RegionX0Pos = x0 - 100;
				data->m_RegionY0Pos = y0 - 100;
				m_RedTIFFData->GetImageRegion(data->m_RegionX0Pos, data->m_RegionY0Pos, 300, 300, m_RedZoomImage);
				m_GreenTIFFData->GetImageRegion(data->m_RegionX0Pos, data->m_RegionY0Pos, 300, 300, m_GreenZoomImage);
				m_BlueTIFFData->GetImageRegion(data->m_RegionX0Pos, data->m_RegionY0Pos, 300, 300, m_BlueZoomImage);
				data->m_RedRegionImage = m_RedZoomImage;
				data->m_GreenRegionImage = m_GreenZoomImage;
				data->m_BlueRegionImage = m_BlueZoomImage;
				data->m_Comment = _T("");
				data->m_HitIndex = i;
				for (int j = 0; j < PATCH_HEIGHT; j++)
				{
					memcpy(m_RedPatchImage + PATCH_WIDTH * j, m_RedZoomImage + (3 * PATCH_WIDTH * (j + 100)) + 100, sizeof(unsigned short) * PATCH_WIDTH);
					memcpy(m_GreenPatchImage + PATCH_WIDTH * j, m_GreenZoomImage + (3 * PATCH_WIDTH * (j + 100)) + 100, sizeof(unsigned short) * PATCH_WIDTH);
					memcpy(m_BluePatchImage + PATCH_WIDTH * j, m_BlueZoomImage + (3 * PATCH_WIDTH * (j + 100)) + 100, sizeof(unsigned short) * PATCH_WIDTH);
				}
				m_RGNDataList[i]->SetImages(m_RedPatchImage, m_GreenPatchImage, m_BluePatchImage);
				bool found = m_HitFinder.ProcessOneRegion(m_RGNDataList[i]);
				m_RGNDataList[i]->NullImages();
				if (found)
				{
					data->Serialize(ctcCapturedArchive);
					*ctcCapturedCount += 1;
					ctcFoundCount++;
					CStdioFile CTCDataFile;
					if (CTCDataFile.Open(_T(".\\CTCCaptured.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
					{
						CTCDataFile.SeekToEnd();
						CString message;
						CRGNData *rgnPtr = m_RGNDataList[i];
						message.Format(_T("%d,%s,%d,%d,%d,%d,%d,%d\n"), *ctcCapturedCount, sampleName, i + 1,
							rgnPtr->GetPixels(RB_COLORS), rgnPtr->GetPixels(RED_COLOR), rgnPtr->m_RedBlueOverlaps, rgnPtr->m_RedValue, rgnPtr->m_GreenValue);
						CTCDataFile.WriteString(message);
						CTCDataFile.Close();
					}
				}
				else
				{
					data->Serialize(ctcNotCapturedArchive);
					*ctcNotCapturedCount += 1;
					CStdioFile CTCDataFile;
					if (CTCDataFile.Open(_T(".\\CTCNotCaptured.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
					{
						CTCDataFile.SeekToEnd();
						CString message;
						CRGNData *rgnPtr = m_RGNDataList[i];
						message.Format(_T("%d,%s,%d\n"), *ctcNotCapturedCount, sampleName, i + 1);
						CTCDataFile.WriteString(message);
						CTCDataFile.Close();
					}
				}
				data->m_RedRegionImage = NULL;
				data->m_GreenRegionImage = NULL;
				data->m_BlueRegionImage = NULL;
				delete data;
			}
		}
		*ctcCount = ctcCount1;
		if (ctcCount1 > 0)
			*ctcCapture = (float)(100.0 * ctcFoundCount / ctcCount1);
		else
			*ctcCapture = 0.0F;
	}
}

BOOL CWBCFinderDlg::LoadRegionData(CString filename, vector<CRGNData *> *regionList)
{
	BOOL ret = FALSE;
	int failureLocation = 0;
	CFile theFile;
	CString message;
	message.Format(_T("Start to Load %s"), filename);
	m_Log.Message(message);
	UINT32 color = 0;
	int index = 0;

	try
	{
		if (theFile.Open(filename, CFile::modeRead))
		{
			failureLocation = 1;
			BYTE* buf = new BYTE[512];
			int count = 0;
			int offset = 0;
			char *ptr = NULL;
			char *ptr1 = NULL;
			int x0 = 0;
			int y0 = 0;

			while (TRUE)
			{
				failureLocation = 2;
				count = theFile.Read(&buf[offset], 1);
				if (count == 1)
				{
					failureLocation = 3;
					if ((buf[offset] == '\0') || (buf[offset] == '\n') || (buf[offset] == '\r'))
					{
						failureLocation = 4;
						if (offset > 0)
						{
							failureLocation = 5;
							if (filename.Find(_T(".rgn"), 0) > 0)
							{
								ptr = strchr((char *)buf, ',');
								if (ptr != NULL)
								{
									failureLocation = 6;
									ptr = strstr(ptr, " 1 ");
									if (ptr != NULL)
									{
										ptr += 3;
										ptr1 = strchr(ptr, ',');
										if (ptr1 != NULL)
										{
											failureLocation = 7;
											*ptr1 = '\0';
											color = (UINT32)atoi(ptr);
											if ((color != CTC) && (color != CTC2))
												color = NONCTC;
											ptr1++;
											ptr = strstr(ptr1, " 2 ");
											if (ptr != NULL)
											{
												failureLocation = 8;
												ptr += 3;
												ptr1 = strchr(ptr, ' ');
												if (ptr1 != NULL)
												{
													failureLocation = 9;
													*ptr1 = '\0';
													x0 = atoi(ptr);
													ptr1++;
													ptr = strchr(ptr1, ',');
													if (ptr != NULL)
													{
														failureLocation = 10;
														*ptr = '\0';
														y0 = atoi(ptr1);
														CRGNData* data = new CRGNData(x0, y0, PATCH_WIDTH, PATCH_HEIGHT);
														data->SetColorCode(color);
														data->SetCPI(RED_COLOR, (int)m_RedTIFFData->GetCPI());
														data->SetCPI(GREEN_COLOR, (int)m_GreenTIFFData->GetCPI());
														data->SetCPI(BLUE_COLOR, (int)m_BlueTIFFData->GetCPI());
														regionList->push_back(data);
														ret = TRUE;
														offset = 0;
														failureLocation = 11;
													}
												}
											}
										}
									}
								}
							}
						}
						else
						{
							if (buf[offset] == '\0')
							{
								failureLocation = 12;
								break;
							}
							else
							{
								failureLocation = 13;
								offset = 0;
								continue;
							}
						}
					}
					else
					{
						failureLocation = 14;
						offset++;
					}
				}
				else
				{
					failureLocation = 15;
					break;
				}
			}

			theFile.Close();
			delete[] buf;
			buf = NULL;
		}
	}
	catch (CException *e)
	{
		TCHAR errCause[255];
		e->GetErrorMessage(errCause, 255);
		CString msg;
		msg.Format(_T("Cought Exception %s, Failre Location = %d"), errCause, failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	catch (...)
	{
		CString msg;
		msg.Format(_T("Cought Exception, Failre Location = %d"), failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	return ret;
}

void CWBCFinderDlg::LoadDefaultSettings()
{
	CStdioFile theFile;
	if (theFile.Open(_T(".\\DefaultSettings.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			if (textline.Find(_T("Leica_Red_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Red_Prefix=");
				Leica_Red_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Green_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Green_Prefix=");
				Leica_Green_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Blue_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Blue_Prefix=");
				Leica_Blue_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Red_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Red_Postfix=");
				Zeiss_Red_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Green_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Green_Postfix=");
				Zeiss_Green_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Blue_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Blue_Postfix=");
				Zeiss_Blue_Postfix = textline.Mid(tag.GetLength());
			}
		}
		theFile.Close();
	}
}

void CWBCFinderDlg::SetStatus(CString message)
{
	CString caption;
	caption.Format(_T("%s - %s"), m_Caption, message);
	SetWindowText(caption);
}

void CWBCFinderDlg::UpdateHitCount(CString filename, int hitCount)
{
	CFile theFile;
	UINT DefaultReadSize = (UINT)4096;

	CFile::Rename(filename, _T(".\\temphit.hit"));
	if (theFile.Open(_T(".\\temphit.hit"), CFile::modeRead))
	{
		CFile outputFile;
		if (outputFile.Open(filename, CFile::modeCreate | CFile::modeWrite))
		{
			UINT fileSize = (UINT)theFile.GetLength();
			BYTE *buf = new BYTE[DefaultReadSize];
			UINT sizeRead = 0;
			UINT sizeToRead = (UINT)DefaultReadSize;
			UINT sizeRemain = fileSize;
			int count = (int)sizeof(int);
			theFile.Read(buf, count);
			outputFile.Write(buf, count);
			theFile.Read(buf, count);
			outputFile.Write(&hitCount, count);
			sizeRemain -= (2 * count);
			while (true)
			{
				if (sizeRemain > DefaultReadSize)
				{
					sizeRead = theFile.Read(buf, sizeToRead);
					if (sizeRead != sizeToRead)
					{
						break;
					}
					else
					{
						outputFile.Write(buf, sizeRead);
						sizeRemain -= DefaultReadSize;
					}
				}
				else if (sizeRemain > (UINT)0)
				{
					sizeToRead = sizeRemain;
					sizeRead = theFile.Read(buf, sizeToRead);
					if (sizeRead != sizeToRead)
					{
						break;
					}
					else
					{
						outputFile.Write(buf, sizeRead);
						sizeRemain = (UINT)0;
						break;
					}
				}
				else
					break;
			}
			outputFile.Close();
		}
		theFile.Close();
	}
	CFile::Remove(_T(".\\temphit.hit"));
}