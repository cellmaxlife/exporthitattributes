#pragma once

#include "ColorType.h"
#include "BlobData.h"

#define PATCH_WIDTH 100
#define PATCH_HEIGHT 100

class CRGNData
{
protected:
	unsigned int m_ColorCode;
	int m_X0;
	int m_Y0;
	int m_Width;
	int m_Height;
	int m_RBPixels;
	int m_GBPixels;
	int m_BPixels;
	int m_RPixels;
	int m_GPixels;
	unsigned short *m_RedImage;
	unsigned short *m_GreenImage;
	unsigned short *m_BlueImage;
	int m_Left;
	int m_Right;
	int m_Top;
	int m_Bottom;
	int m_RedCPI;
	int m_GreenCPI;
	int m_BlueCPI;
	vector<CBlobData *> *m_RedBlobs;
	vector<CBlobData *> *m_GreenBlobs;
	vector<CBlobData *> *m_BlueBlobs;
	vector<CBlobData *> *m_GreenRingBlobs;
	vector<CBlobData *> *m_RedBlueBlobs;

public:
	CRGNData(int x0, int y0, int width, int height);
	CRGNData(CRGNData *region);
	virtual ~CRGNData();
	void SetColorCode(unsigned int colorCode);
	unsigned int GetColorCode();
	void GetPosition(int *x0, int *y0);
	void SetPosition(int x0, int y0);
	void SetPixels(PIXEL_COLOR_TYPE color, int pixels);
	void SetCPI(PIXEL_COLOR_TYPE color, int CPI);
	int GetPixels(PIXEL_COLOR_TYPE color);
	int GetCPI(PIXEL_COLOR_TYPE color);
	int GetWidth();
	int GetHeight();
	void SetBoundingBox(int left, int top, int right, int bottom);
	void GetBoundingBox(int *left, int *top, int *right, int *bottom);
	void SetImages(unsigned short *redImage, unsigned short *greenImage, unsigned short *blueImage);
	void NullImages();
	unsigned short *GetImage(PIXEL_COLOR_TYPE color);
	int m_AspectRatio;
	int m_HitIndex;
	int m_GreenAverage;
	int m_GreenMax;
	int m_RedAverage;
	int m_RedMax;
	int m_BlueAverage;
	int m_BlueMax;
	vector<CBlobData *> *GetBlobData(PIXEL_COLOR_TYPE color);
	void FreeBlobList(PIXEL_COLOR_TYPE color);
	int m_RedValue;
	int m_GreenValue;
	void CopyRegionData(CRGNData *data);
	void CopyBlobList(vector<CBlobData *> *dest, vector<CBlobData *> *src);
	int m_RedFrameMax;
	int m_GreenFrameMax;
	int m_BlueFrameMax;
	int m_Extent;
	int m_LongAxisLength;
	int m_BlueValue;
	void SetFrameMax(PIXEL_COLOR_TYPE color, int maxInten);
	int m_RedBlueOverlaps;
	int m_GreenBlueOverlaps;
};
