#include "stdafx.h"
#include "RGNData.h"

CRGNData::CRGNData(int x0, int y0, int width, int height)
{
	m_X0 = x0;
	m_Y0 = y0;
	m_Width = width;
	m_Height = height;
	m_ColorCode = 0;
	m_RBPixels = 0;
	m_GBPixels = 0;
	m_BPixels = 0;
	m_RPixels = 0;
	m_GPixels = 0;
	m_RedImage = NULL;
	m_GreenImage = NULL;
	m_BlueImage = NULL;
	m_Left = 0;
	m_Right = 100;
	m_Top = 0;
	m_Bottom = 100;
	m_RedCPI = 200;
	m_GreenCPI = 200;
	m_BlueCPI = 200;
	m_AspectRatio = 0;
	m_HitIndex = 0;
	m_GreenAverage = 0;
	m_GreenMax = 0;
	m_RedAverage = 0;
	m_RedMax = 0;
	m_BlueAverage = 0;
	m_BlueMax = 0;
	m_RedBlobs = new vector<CBlobData *>();
	m_GreenBlobs = new vector<CBlobData *>();
	m_BlueBlobs = new vector<CBlobData *>();
	m_GreenRingBlobs = new vector<CBlobData *>();
	m_RedBlueBlobs = new vector<CBlobData *>();
	m_RedValue = 0;
	m_GreenValue = 0;
	m_RedFrameMax = 0;
	m_GreenFrameMax = 0;
	m_BlueFrameMax = 0;
	m_Extent = 0;
	m_LongAxisLength = 0;
	m_BlueValue = 0;
	m_RedBlueOverlaps = 0;
	m_GreenBlueOverlaps = 0;
}

CRGNData::CRGNData(CRGNData *region)
{
	int x0, y0;
	region->GetPosition(&x0, &y0);
	m_X0 = x0;
	m_Y0 = y0;
	m_Width = region->GetWidth();
	m_Height = region->GetHeight();
	m_ColorCode = 0;
	m_RBPixels = 0;
	m_GBPixels = 0;
	m_BPixels = 0;
	m_RPixels = 0;
	m_GPixels = 0;
	m_RedImage = new unsigned short[m_Width * m_Height];
	memcpy(m_RedImage, region->GetImage(RED_COLOR), sizeof(unsigned short) * m_Width * m_Height);
	m_GreenImage = new unsigned short[m_Width * m_Height];
	memcpy(m_GreenImage, region->GetImage(GREEN_COLOR), sizeof(unsigned short) * m_Width * m_Height);
	m_BlueImage = new unsigned short[m_Width * m_Height];
	memcpy(m_BlueImage, region->GetImage(BLUE_COLOR), sizeof(unsigned short) * m_Width * m_Height);
	m_Left = 0;
	m_Right = 0;
	m_Top = 0;
	m_Bottom = 0;
	m_RedCPI = region->GetCPI(RED_COLOR);
	m_GreenCPI = region->GetCPI(GREEN_COLOR);
	m_BlueCPI = region->GetCPI(BLUE_COLOR);
	m_HitIndex = 0;
	m_GreenAverage = 0;
	m_RedAverage = 0;
	m_BlueAverage = 0;
	m_RedBlobs = new vector<CBlobData *>();
	m_GreenBlobs = new vector<CBlobData *>();
	m_BlueBlobs = new vector<CBlobData *>();
	m_GreenRingBlobs = new vector<CBlobData *>();
	m_RedBlueBlobs = new vector<CBlobData *>();
	m_RedValue = 0;
	m_GreenValue = 0;
	m_RedFrameMax = 0;
	m_GreenFrameMax = 0;
	m_BlueFrameMax = 0;
	m_BlueValue = 0;
	m_RedBlueOverlaps = 0;
	m_GreenBlueOverlaps = 0;
	m_AspectRatio = 0;
	m_Extent = 0;
	m_LongAxisLength = 0;
}

CRGNData::~CRGNData()
{
	if (m_RedImage != NULL)
		delete m_RedImage;
	if (m_GreenImage != NULL)
		delete m_GreenImage;
	if (m_BlueImage != NULL)
		delete m_BlueImage;
	FreeBlobList(RED_COLOR);
	delete m_RedBlobs;
	FreeBlobList(GREEN_COLOR);
	delete m_GreenBlobs;
	FreeBlobList(BLUE_COLOR);
	delete m_BlueBlobs;
	FreeBlobList(GB_COLORS);
	delete m_GreenRingBlobs;
	FreeBlobList(RB_COLORS);
	delete m_RedBlueBlobs;
}

void CRGNData::SetColorCode(unsigned int colorCode)
{
	m_ColorCode = colorCode;
}

unsigned int CRGNData::GetColorCode()
{
	return m_ColorCode;
}

void CRGNData::GetPosition(int *x0, int *y0)
{
	*x0 = m_X0;
	*y0 = m_Y0;
}

void CRGNData::SetPixels(PIXEL_COLOR_TYPE color, int pixels)
{
	switch (color)
	{
	case RB_COLORS:
		m_RBPixels = pixels;
		break;
	case GB_COLORS:
		m_GBPixels = pixels;
		break;
	case RED_COLOR:
		m_RPixels = pixels;
		break;
	case GREEN_COLOR:
		m_GPixels = pixels;
		break;
	case BLUE_COLOR:
		m_BPixels = pixels;
		break;
	}
}

int CRGNData::GetPixels(PIXEL_COLOR_TYPE color)
{
	int pixels = 0;
	switch (color)
	{
	case RB_COLORS:
		pixels = m_RBPixels;
		break;
	case GB_COLORS:
		pixels = m_GBPixels;
		break;
	case RED_COLOR:
		pixels = m_RPixels;
		break;
	case GREEN_COLOR:
		pixels = m_GPixels;
		break;
	case BLUE_COLOR:
		pixels = m_BPixels;
		break;
	}
	return pixels;
}

void CRGNData::SetImages(unsigned short *redImage, unsigned short *greenImage, unsigned short *blueImage)
{
	if (m_RedImage != NULL)
	{
		delete m_RedImage;
		m_RedImage = NULL;
	}
	m_RedImage = redImage;
	if (m_GreenImage != NULL)
	{
		delete m_GreenImage;
		m_GreenImage = NULL;
	}
	m_GreenImage = greenImage;
	if (m_BlueImage != NULL)
	{
		delete m_BlueImage;
		m_BlueImage = NULL;
	}
	m_BlueImage = blueImage;
}

void CRGNData::NullImages()
{
	m_RedImage = NULL;
	m_GreenImage = NULL;
	m_BlueImage = NULL;
}

unsigned short *CRGNData::GetImage(PIXEL_COLOR_TYPE color)
{
	unsigned short *image = NULL;
	switch (color)
	{
	case RB_COLORS:
		break;
	case GB_COLORS:
		break;
	case RED_COLOR:
		image = m_RedImage;
		break;
	case GREEN_COLOR:
		image = m_GreenImage;
		break;
	case BLUE_COLOR:
		image = m_BlueImage;
		break;
	}
	return image;
}

int CRGNData::GetWidth()
{
	return m_Width;
}

int CRGNData::GetHeight()
{
	return m_Height;
}

void CRGNData::SetBoundingBox(int left, int top, int right, int bottom)
{
	m_Left = left;
	m_Top = top;
	m_Right = right;
	m_Bottom = bottom;
}

void CRGNData::GetBoundingBox(int *left, int *top, int *right, int *bottom)
{
	*left = m_Left;
	*top = m_Top;
	*right = m_Right;
	*bottom = m_Bottom;
}

void CRGNData::SetPosition(int x, int y)
{
	m_X0 = x;
	m_Y0 = y;
}

void CRGNData::SetCPI(PIXEL_COLOR_TYPE color, int CPI)
{
	switch (color)
	{
	case RB_COLORS:
		break;
	case GB_COLORS:
		break;
	case RED_COLOR:
		m_RedCPI = CPI;
		break;
	case GREEN_COLOR:
		m_GreenCPI = CPI;
		break;
	case BLUE_COLOR:
		m_BlueCPI = CPI;
		break;
	}
}

int CRGNData::GetCPI(PIXEL_COLOR_TYPE color)
{
	int CPI = 0;
	switch (color)
	{
	case RB_COLORS:
		break;
	case GB_COLORS:
		break;
	case RED_COLOR:
		CPI = m_RedCPI;
		break;
	case GREEN_COLOR:
		CPI = m_GreenCPI;
		break;
	case BLUE_COLOR:
		CPI = m_BlueCPI;
		break;
	}
	return CPI;
}

void CRGNData::FreeBlobList(PIXEL_COLOR_TYPE color)
{
	vector<CBlobData *> *ptr = NULL;
	switch (color)
	{
	case RB_COLORS:
		ptr = m_RedBlueBlobs;
		break;
	case GB_COLORS:
		ptr = m_GreenRingBlobs;
		break;
	case RED_COLOR:
		ptr = m_RedBlobs;
		break;
	case GREEN_COLOR:
		ptr = m_GreenBlobs;
		break;
	case BLUE_COLOR:
		ptr = m_BlueBlobs;
		break;
	}
	for (int i = 0; i < (int)ptr->size(); i++)
	{
		if ((*ptr)[i] != NULL)
		{
			delete (*ptr)[i];
			(*ptr)[i] = NULL;
		}
	}
	ptr->clear();
}

vector<CBlobData *> *CRGNData::GetBlobData(PIXEL_COLOR_TYPE color)
{
	vector<CBlobData *> *ptr = NULL;
	switch (color)
	{
	case RB_COLORS:
		ptr = m_RedBlueBlobs;
		break;
	case GB_COLORS:
		ptr = m_GreenRingBlobs;
		break;
	case RED_COLOR:
		ptr = m_RedBlobs;
		break;
	case GREEN_COLOR:
		ptr = m_GreenBlobs;
		break;
	case BLUE_COLOR:
		ptr = m_BlueBlobs;
		break;
	}
	return ptr;
}

void CRGNData::CopyRegionData(CRGNData *data)
{
	m_ColorCode = data->GetColorCode();
	m_RBPixels = data->GetPixels(RB_COLORS);
	m_GBPixels = data->GetPixels(GB_COLORS);
	m_BPixels = data->GetPixels(BLUE_COLOR);
	m_RPixels = data->GetPixels(RED_COLOR);
	m_GPixels = data->GetPixels(GREEN_COLOR);
	int left, top, right, bottom;
	data->GetBoundingBox(&left, &top, &right, &bottom);
	m_Left = left;
	m_Right = right;
	m_Top = top;
	m_Bottom = bottom;
	m_GreenAverage = data->m_GreenAverage;
	m_GreenMax = data->m_GreenMax;
	m_RedAverage = data->m_RedAverage;
	m_RedMax = data->m_RedMax;
	m_BlueAverage = data->m_BlueAverage;
	m_BlueMax = m_BlueMax;
	FreeBlobList(RED_COLOR);
	FreeBlobList(GREEN_COLOR);
	FreeBlobList(BLUE_COLOR);
	FreeBlobList(RB_COLORS);
	FreeBlobList(GB_COLORS);
	CopyBlobList(m_RedBlobs, data->GetBlobData(RED_COLOR));
	CopyBlobList(m_GreenBlobs, data->GetBlobData(GREEN_COLOR));
	CopyBlobList(m_BlueBlobs, data->GetBlobData(BLUE_COLOR));
	CopyBlobList(m_GreenRingBlobs, data->GetBlobData(GB_COLORS));
	CopyBlobList(m_RedBlueBlobs, data->GetBlobData(RB_COLORS));
	m_RedValue = data->m_RedValue;
	m_LongAxisLength = data->m_LongAxisLength;
	m_AspectRatio = data->m_AspectRatio;
	m_Extent = data->m_Extent;
	m_GreenValue = data->m_GreenValue;
	m_BlueValue = data->m_BlueValue;
	m_RedFrameMax = data->m_RedFrameMax;
	m_GreenFrameMax = data->m_GreenFrameMax;
	m_BlueFrameMax = data->m_BlueFrameMax;
	m_RedCPI = data->GetCPI(RED_COLOR);
	m_GreenCPI = data->GetCPI(GREEN_COLOR);
	m_BlueCPI = data->GetCPI(BLUE_COLOR);
	m_RedBlueOverlaps = data->m_RedBlueOverlaps;
	m_GreenBlueOverlaps = data->m_GreenBlueOverlaps;
}

void CRGNData::CopyBlobList(vector<CBlobData *> *dest, vector<CBlobData *> *src)
{
	for (int i = 0; i < (int)src->size(); i++)
	{
		dest->push_back((*src)[i]);
		(*src)[i] = NULL;
	}
	src->clear();
}
