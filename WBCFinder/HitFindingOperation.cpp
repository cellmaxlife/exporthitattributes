#include "stdafx.h"
#include "HitFindingOperation.h"
#include <string.h>
#include <math.h>
#include <thread>

#define MIN_ARTIFACT_PIXEL_COUNT	1000
#define MAX_BLOB_PIXEL_COUNT		1500
#define THRESHOLD_INCREMENT_STEP	50

CHitFindingOperation::CHitFindingOperation()
{
}

CHitFindingOperation::~CHitFindingOperation()
{
}

void CHitFindingOperation::FreeBlobList(vector<CBlobData *> *blobList)
{
	for (int i = 0; i < (int)blobList->size(); i++)
	{
		if ((*blobList)[i] != NULL)
		{
			delete (*blobList)[i];
			(*blobList)[i] = NULL;
		}
	}
	blobList->clear();
}

void CHitFindingOperation::FreeBlobTree(vector<CBlobTreeNode *> *blobTree)
{
	for (int i = 0; i < (int)blobTree->size(); i++)
	{
		if ((*blobTree)[i] != NULL)
		{
			delete (*blobTree)[i];
			(*blobTree)[i] = NULL;
		}
	}
	blobTree->clear();
}

void CHitFindingOperation::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	rgnList->clear();
}

template<typename T>
void CHitFindingOperation::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

int CHitFindingOperation::Partition(vector<int> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	int pos = (*list)[position];
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i] < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CHitFindingOperation::ChoosePivot(vector<int> *list, int lo, int hi)
{
	int pivot = lo;
	int pos0 = (*list)[lo];
	int pos1 = (*list)[(lo + hi) / 2];
	int pos2 = (*list)[hi];
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

template<typename T>
void CHitFindingOperation::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}

void CHitFindingOperation::CalculateBoundary(vector<CBlobData *> *blobList)
{
	for (int i = 0; i < (int)blobList->size(); i++)
	{
		GetBlobBoundary((*blobList)[i]);
	}
}

void CHitFindingOperation::ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height)
{
	memset(outImage, 0, sizeof(unsigned char) * width * height);

	for (int i = 1; i < (height - 1); i++)
	{
		for (int j = 1; j < (width - 1); j++)
		{
			bool keep = true;
			for (int k = -1; k < 2; k++)
			{
				for (int h = -1; h < 2; h++)
				{
					if (inImage[width * (i + k) + (j + h)] != (unsigned char)255)
					{
						keep = false;
						break;
					}
				}
				if (!keep)
					break;
			}
			if (keep)
				outImage[width * i + j] = (unsigned char)255;
		}
	}
}

void CHitFindingOperation::GetBlobBoundary(CBlobData *blob)
{
	unsigned char *temp1 = new unsigned char[blob->m_Width * blob->m_Height];
	unsigned char *temp2 = new unsigned char[blob->m_Width * blob->m_Height];
	memset(temp2, 0, sizeof(unsigned char) * blob->m_Width * blob->m_Height);
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		int x = (int)((*blob->m_Pixels)[i] % blob->m_Width);
		int y = (int)((*blob->m_Pixels)[i] / blob->m_Width);
		temp2[blob->m_Width * y + x] = (unsigned char)255;
	}
	ErosionOperation(temp2, temp1, blob->m_Width, blob->m_Height);
	blob->m_Boundary->clear();
	for (int i = 0; i < blob->m_Height; i++)
	{
		for (int j = 0; j < blob->m_Width; j++)
		{
			if ((temp2[blob->m_Width * i + j] == (unsigned char)255) &&
				(temp1[blob->m_Width * i + j] == (unsigned char)0))
			{
				int pos = (unsigned int)(i * blob->m_Width + j);
				blob->m_Boundary->push_back(pos);
			}
		}
	}
	delete[] temp1;
	delete[] temp2;
}

bool CHitFindingOperation::ProcessOneRegion(CRGNData *region)
{
	vector<CRGNData *> regionList;
	bool ret = ProcessOnePatch(region, &regionList);
	if (ret)
	{
		int maxIndex = -1;
		int maxRedIntensitySum = 0;
		for (int i = 0; i < (int)regionList.size(); i++)
		{
			int redIntensitySum = regionList[i]->GetPixels(RED_COLOR) * regionList[i]->m_RedValue;
			if (redIntensitySum > maxRedIntensitySum)
			{
				maxRedIntensitySum = redIntensitySum;
				maxIndex = i;
			}
		}
		if (maxIndex > -1)
		{
			region->CopyRegionData(regionList[maxIndex]);
			CalculateBoundary(region->GetBlobData(RED_COLOR));
			CalculateBoundary(region->GetBlobData(GREEN_COLOR));
			CalculateBoundary(region->GetBlobData(BLUE_COLOR));
			CalculateBoundary(region->GetBlobData(RB_COLORS));
		}
	}
	FreeRGNList(&regionList);
	return ret;
}

int CHitFindingOperation::GetOverlapPixelCount(vector<int> *blob1, vector<int> *blob2, vector<int> *blob3)
{
	int count = 0;
	int index1 = 0;
	int index2 = 0;
	while ((index1 < (int)blob1->size()) && (index2 < (int)blob2->size()))
	{
		if ((*blob1)[index1] < (*blob2)[index2])
			index1++;
		else if ((*blob1)[index1] > (*blob2)[index2])
		{
			blob3->push_back((*blob2)[index2]);
			index2++;
		}
		else
		{
			count++;
			index1++;
			index2++;
		}
	}
	return count;
}

bool CHitFindingOperation::blobIncluded(vector<int> *list, int blobIndex)
{
	bool ret = false;
	for (int i = 0; i < (int)list->size(); i++)
	{
		if (blobIndex == (*list)[i])
		{
			ret = true;
			break;
		}
	}

	return ret;
}

bool CHitFindingOperation::MergeBlobsToGetCTC(CRGNData *region, vector<CRGNData *> *rgnList)
{
	vector<CRGNData *> hitList;
	vector<int> redBlobs;
	vector<int> greenBlobs;
	vector<int> blueBlobs;
	for (int i = 0; i < (int)region->GetBlobData(RED_COLOR)->size(); i++)
	{
		CBlobData *redBlob = (*region->GetBlobData(RED_COLOR))[i];
		if (redBlob == NULL)
			continue;
		redBlobs.clear();
		greenBlobs.clear();
		blueBlobs.clear();
		int OverlappedRedBluePixelCount = 0;
		redBlobs.push_back(i);
		vector<int> RBBlobPixels;
		for (int j = 0; j < (int)redBlob->m_Pixels->size(); j++)
			RBBlobPixels.push_back((*redBlob->m_Pixels)[j]);
		bool newPixelAdded = true;
		while (newPixelAdded)
		{
			newPixelAdded = false;
			for (int j = 0; j < (int)region->GetBlobData(BLUE_COLOR)->size(); j++)
			{
				CBlobData *blueBlob = (*region->GetBlobData(BLUE_COLOR))[j];
				if (blueBlob == NULL)
					continue;
				if (blobIncluded(&blueBlobs, j))
					continue;
				vector<int> notOverlappedBluePixels;
				int overlappedPixels = GetOverlapPixelCount(&RBBlobPixels, blueBlob->m_Pixels, &notOverlappedBluePixels);
				if (overlappedPixels > 0)
				{
					newPixelAdded = true;
					OverlappedRedBluePixelCount += overlappedPixels;
					blueBlobs.push_back(j);
					for (int k = 0; k < (int)notOverlappedBluePixels.size(); k++)
						RBBlobPixels.push_back(notOverlappedBluePixels[k]);
					QuickSort(&RBBlobPixels, 0, (int)RBBlobPixels.size() - 1);
				}
				notOverlappedBluePixels.clear();
			}
			if (newPixelAdded)
			{
				newPixelAdded = false;
				for (int j = 0; j < (int)region->GetBlobData(RED_COLOR)->size(); j++)
				{
					CBlobData *redBlob1 = (*region->GetBlobData(RED_COLOR))[j];
					if (redBlob1 == NULL)
						continue;
					if (blobIncluded(&redBlobs, j))
						continue;
					vector<int> notOverlappedRedPixels;
					int overlappedPixels = GetOverlapPixelCount(&RBBlobPixels, redBlob1->m_Pixels, &notOverlappedRedPixels);
					if (overlappedPixels > 0)
					{
						newPixelAdded = true;
						OverlappedRedBluePixelCount += overlappedPixels;
						redBlobs.push_back(j);
						for (int k = 0; k < (int)notOverlappedRedPixels.size(); k++)
							RBBlobPixels.push_back(notOverlappedRedPixels[k]);
						QuickSort(&RBBlobPixels, 0, (int)RBBlobPixels.size() - 1);
					}
					notOverlappedRedPixels.clear();
				}
			}
		}
		if (OverlappedRedBluePixelCount > 0)
		{
			int x0, y0;
			region->GetPosition(&x0, &y0);
			CRGNData *newHit = new CRGNData(x0, y0, region->GetWidth(), region->GetHeight());
			newHit->SetCPI(RED_COLOR, region->GetCPI(RED_COLOR));
			newHit->SetCPI(GREEN_COLOR, region->GetCPI(GREEN_COLOR));
			newHit->SetCPI(BLUE_COLOR, region->GetCPI(BLUE_COLOR));
			hitList.push_back(newHit);
			newHit->m_RedBlueOverlaps = OverlappedRedBluePixelCount;
			CBlobData *rbBlob = new CBlobData(region->GetWidth(), region->GetHeight());
			newHit->GetBlobData(RB_COLORS)->push_back(rbBlob);
			for (int j = 0; j < (int)RBBlobPixels.size(); j++)
				rbBlob->m_Pixels->push_back(RBBlobPixels[j]);
			for (int j = 0; j < (int)region->GetBlobData(RED_COLOR)->size(); j++)
			{
				CBlobData *redBlob1 = (*region->GetBlobData(RED_COLOR))[j];
				if ((redBlob1 != NULL) && (blobIncluded(&redBlobs, j)))
				{
					newHit->GetBlobData(RED_COLOR)->push_back(redBlob1);
					(*region->GetBlobData(RED_COLOR))[j] = NULL;
				}
			}
			for (int j = 0; j < (int)region->GetBlobData(BLUE_COLOR)->size(); j++)
			{
				CBlobData *blueBlob1 = (*region->GetBlobData(BLUE_COLOR))[j];
				if ((blueBlob1 != NULL) && (blobIncluded(&blueBlobs, j)))
				{
					newHit->GetBlobData(BLUE_COLOR)->push_back(blueBlob1);
					(*region->GetBlobData(BLUE_COLOR))[j] = NULL;
				}
			}
			for (int j = 0; j < (int)region->GetBlobData(GREEN_COLOR)->size(); j++)
			{
				CBlobData *greenBlob1 = (*region->GetBlobData(GREEN_COLOR))[j];
				if (greenBlob1 == NULL)
					continue;
				vector<int> notOverlappedGreenPixels;
				int overlappedPixels = GetOverlapPixelCount(&RBBlobPixels, greenBlob1->m_Pixels, &notOverlappedGreenPixels);
				if (overlappedPixels > 0)
				{
					newHit->m_GreenBlueOverlaps += overlappedPixels;
					newHit->GetBlobData(GREEN_COLOR)->push_back(greenBlob1);
					(*region->GetBlobData(GREEN_COLOR))[j] = NULL;
				}
				notOverlappedGreenPixels.clear();
			}
			RBBlobPixels.clear();
		}
	}
	for (int i = 0; i < (int)hitList.size(); i++)
	{
		CalculateHitAttributes(hitList[i]);
		rgnList->push_back(hitList[i]);
		hitList[i] = NULL;
	}
	bool ret = false;
	if (hitList.size() > 0)
		ret = true;
	FreeRGNList(&hitList);
	return ret;
}

int CHitFindingOperation::GetPixelCountSum(CRGNData *region, PIXEL_COLOR_TYPE color)
{
	int pixelCount = 0;

	for (int i = 0; i < (int)region->GetBlobData(color)->size(); i++)
		pixelCount += (int)(*region->GetBlobData(color))[i]->m_Pixels->size();

	return pixelCount;
}

int CHitFindingOperation::GetAverageIntensity(CRGNData *region, PIXEL_COLOR_TYPE color)
{
	int IntensitySum = 0;
	int PixelCount = 0;
	int maxIntensity = 255;
	for (int i = 0; i < (int)region->GetBlobData(color)->size(); i++)
	{
		PixelCount += (int)(*region->GetBlobData(color))[i]->m_Pixels->size();
		for (int j = 0; j < (int)(*region->GetBlobData(color))[i]->m_Pixels->size(); j++)
		{
			int intensity = (int)(*(*region->GetBlobData(color))[i]->m_Intensities)[j];
			IntensitySum += intensity;
			if (intensity > maxIntensity)
				maxIntensity = intensity;
		}
	}
	int Average = 0;
	if (PixelCount > 0)
		Average = IntensitySum / PixelCount;
	if (color == RED_COLOR)
		region->m_RedFrameMax = maxIntensity;
	else if (color == GREEN_COLOR)
		region->m_GreenFrameMax = maxIntensity;
	else
		region->m_BlueAverage = maxIntensity;
	return Average;
}

void CHitFindingOperation::CalculateHitAttributes(CRGNData *region)
{
	region->SetPixels(RED_COLOR, GetPixelCountSum(region, RED_COLOR));
	region->SetPixels(GREEN_COLOR, GetPixelCountSum(region, GREEN_COLOR));
	region->SetPixels(BLUE_COLOR, GetPixelCountSum(region, BLUE_COLOR));
	region->SetPixels(RB_COLORS, GetPixelCountSum(region, RB_COLORS));
	region->SetPixels(GB_COLORS, GetPixelCountSum(region, GB_COLORS));
	region->m_RedAverage = GetAverageIntensity(region, RED_COLOR);
	region->m_GreenAverage = GetAverageIntensity(region, GREEN_COLOR);
	region->m_BlueAverage = GetAverageIntensity(region, BLUE_COLOR);

	if (region->m_RedAverage > region->GetCPI(RED_COLOR))
		region->m_RedValue = region->m_RedAverage - region->GetCPI(RED_COLOR);
	else
		region->m_RedValue = 0;
	if (region->m_GreenAverage > region->GetCPI(GREEN_COLOR))
		region->m_GreenValue = region->m_GreenAverage - region->GetCPI(GREEN_COLOR);
	else
		region->m_GreenValue = 0;
}

void CHitFindingOperation::FindBlobs(unsigned short *image, vector<CBlobData *> *blobs, int *CPI)
{
	int IntensitySum = 0;
	int PixelCount = 0;
	vector<INTENSITY_PIXEL_SET *> pixelIntensityList;
	SortPixelIntensities(image, &pixelIntensityList);
	if (pixelIntensityList.size() > 0)
	{
		int maxCount = 0;
		int maxIndex = -1;
		for (int i = 0; i < (int)pixelIntensityList.size(); i++)
		{
			if ((int)pixelIntensityList[i]->pixelList->size() > maxCount)
			{
				maxIndex = i;
				maxCount = (int)pixelIntensityList[i]->pixelList->size();
			}
		}
		if (maxCount > MIN_ARTIFACT_PIXEL_COUNT)
		{
			for (int i = 0; i < (int)pixelIntensityList[maxIndex]->pixelList->size(); i++)
			{
				image[(*pixelIntensityList[maxIndex]->pixelList)[i]] = (unsigned short)0;
			}
			pixelIntensityList[maxIndex]->intensity = 0;
		}
		for (int i = 0; i< (int)pixelIntensityList.size(); i++)
		{
			PixelCount += (int)pixelIntensityList[i]->pixelList->size();
			IntensitySum += (pixelIntensityList[i]->intensity * (int)pixelIntensityList[i]->pixelList->size());
		}
		for (int i = 0; i < (int)pixelIntensityList.size(); i++)
		{
			delete pixelIntensityList[i];
			pixelIntensityList[i] = NULL;
		}
		pixelIntensityList.clear();
	}
	
	if (PixelCount == 0)
		return;

	int threshold = (IntensitySum / PixelCount) + THRESHOLD_INCREMENT_STEP;
	*CPI = threshold;

	CFindConnectedPixels *finder = new CFindConnectedPixels();
	vector<CBlobData *> blobList;

	finder->FindConnectedPixels(image, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, threshold, &blobList);
	delete finder;
	finder = NULL;

	if (blobList.size() > 0)
	{
		vector<CBlobTreeNode *> blobTree;
		for (int i = 0; i < (int)blobList.size(); i++)
		{
			CBlobTreeNode *node = new CBlobTreeNode(blobList[i], threshold);
			blobTree.push_back(node);
			if ((int)blobList[i]->m_Pixels->size() > MAX_BLOB_PIXEL_COUNT)
				FindBlob(node);
			blobList[i] = NULL;
		}
		blobList.clear();
		vector<CBlobTreeNode *> leafNodes;
		PickLeafNodes(&blobTree, &leafNodes);
		for (int i = 0; i < (int)leafNodes.size(); i++)
		{
			blobs->push_back(leafNodes[i]->m_Node);
			leafNodes[i]->m_Node = NULL;
		}
		leafNodes.clear();
		FreeBlobTree(&blobTree);
	}
}

void CHitFindingOperation::PickLeafNodes(vector<CBlobTreeNode *> *treeHead, vector<CBlobTreeNode *> *leafNodes)
{
	for (int i = 0; i < (int)treeHead->size(); i++)
	{
		if (((*treeHead)[i]->m_Children != NULL) && ((*treeHead)[i]->m_Children->size() > 0))
		{
			PickLeafNodes((*treeHead)[i]->m_Children, leafNodes);
		}
		else
		{
			leafNodes->push_back((*treeHead)[i]);
		}
	}
}

void CHitFindingOperation::FindBlob(CBlobTreeNode *node)
{
	CFindConnectedPixels *finder = new CFindConnectedPixels();
	vector<CBlobData *> blobList;
	int threshold = node->m_Threshold + THRESHOLD_INCREMENT_STEP;
	finder->FindConnectedPixels(node->m_Node, threshold, &blobList);
	delete finder;
	finder = NULL;
	if ((int)blobList.size() > 0)
	{
		node->m_Children = new vector<CBlobTreeNode *>();
		for (int i = 0; i < (int)blobList.size(); i++)
		{
			CBlobTreeNode *child = new CBlobTreeNode(blobList[i], threshold);
			node->m_Children->push_back(child);
			if ((int)blobList[i]->m_Pixels->size() > MAX_BLOB_PIXEL_COUNT)
				FindBlob(child);
			blobList[i] = NULL;
		}
		blobList.clear();
	}
}

void CHitFindingOperation::SortPixelIntensities(unsigned short *image, vector<INTENSITY_PIXEL_SET *> *sortedList)
{
	int length = SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT;
	unsigned short *imgPtr = image;
	for (int i = 0; i < length; i++, imgPtr++)
	{
		int intensity = (int)*imgPtr;
		AddOnePixel(sortedList, *imgPtr, i);
	}
}

void CHitFindingOperation::AddOnePixel(vector<INTENSITY_PIXEL_SET *> *list, unsigned short intensity, int position)
{
	vector<INTENSITY_PIXEL_SET *>::iterator it0;
	for (it0 = list->begin(); it0 != list->end(); it0++)
	{
		if (intensity <= (*it0)->intensity)
			break;
	}
	if (it0 == list->end())
	{
		INTENSITY_PIXEL_SET *newSet = new INTENSITY_PIXEL_SET(intensity, position);
		list->push_back(newSet);
	}
	else if ((*it0)->intensity == intensity)
	{
		(*it0)->pixelList->push_back(position);
	}
	else
	{
		INTENSITY_PIXEL_SET *newSet = new INTENSITY_PIXEL_SET(intensity, position);
		list->insert(it0, newSet);
	}
}

bool CHitFindingOperation::ProcessOnePatch(CRGNData *region, vector<CRGNData *> *rgnList)
{
	FreeBlobList(region->GetBlobData(RED_COLOR));
	FreeBlobList(region->GetBlobData(GREEN_COLOR));
	FreeBlobList(region->GetBlobData(BLUE_COLOR));
	FreeBlobList(region->GetBlobData(RB_COLORS));
	FreeBlobList(region->GetBlobData(GB_COLORS));

	int redCPI = 0;
	int greenCPI = 0;
	int blueCPI = 0;

	std::thread redThread1(FindBlobs, region->GetImage(RED_COLOR), region->GetBlobData(RED_COLOR), &redCPI);
	std::thread greenThread1(FindBlobs, region->GetImage(GREEN_COLOR), region->GetBlobData(GREEN_COLOR), &greenCPI);
	std::thread blueThread1(FindBlobs, region->GetImage(BLUE_COLOR), region->GetBlobData(BLUE_COLOR), &blueCPI);

	redThread1.join();
	greenThread1.join();
	blueThread1.join();

	region->SetCPI(RED_COLOR, redCPI);
	region->SetCPI(GREEN_COLOR, greenCPI);
	region->SetCPI(BLUE_COLOR, blueCPI);

	bool ret = MergeBlobsToGetCTC(region, rgnList);

	return ret;
}